@echo off && cls

call rollup -i source/index.js -o build/murakushi.js -f umd -n murakushi
call uglifyjs -c -m -o  build/murakushi.min.js --  build/murakushi.js
