<<<<<<< HEAD:definition.d.ts
declare namespace murakushi {

    export interface Vector2 {
        length: 2;
        x: number; y: number; [index: number]: number;
        set(x: number, y: number): this;
        reset(): this;
        readonly norm: number;
        readonly normSquared: number;
    }

    export namespace Vector2 {
        export function create(x: number, y: number): Vector2;
        export const Zero: Vector2;
        export const UnitX: Vector2;
        export const UnitY: Vector2;
        export function add(out: Vector2, a: Vector2, b: Vector2): Vector2;
        export function sub(out: Vector2, a: Vector2, b: Vector2): Vector2;
        export function div(out: Vector2, a: Vector2, b: Vector2): Vector2;
        export function mul(out: Vector2, a: Vector2, b: Vector2): Vector2;
        export function scale(out: Vector2, u: Vector2, scale: number): Vector2;
        export function iterate(out: Vector2, x: Vector2, dx: Vector2, dt: number): Vector2;
        export function dist(a: Vector2, b: Vector2): number;
        export function check_dist(dist: number, a: Vector2, b: Vector2): boolean;
        export function dot(a: Vector2, b: Vector2): number;
        export function lerp(out: Vector2, a: Vector2, b: Vector2): Vector2;
        export function normalize(out: Vector2, u: Vector2): Vector2;
    }

    export interface Vector3 {
        length: 3;
        x: number; y: number; z: number; [index: number]: number;
        set(x: number, y: number, z: number): this;
        reset(): this;
        readonly norm: number;
        readonly normSquared: number;
    }
=======
declare module murakushi {

    //#region vec2 defintion

    export interface vec2 extends Float32Array {

        /**
         * @param x X coordinate.
         * @param y Y coordinate.
         * @returns the current instance.
         */
        setCoords(x: number, y: number): this;

        /**
         * Resets the coordinates to zero.
         * @returns the current instance.
         */
        resetCoords(): this;

        /**
         * Clones this vector, creating a new instance.
         * @returns the new vector.
         */
        clone(): this;

        /**
         * Adds a scaled vector, useful in integration.
         * @param u Other vector.
         * @param q Scale factor.
         * @returns This vector.
         */
        addScaled(u: vec2, q: number): this;

        /**
         * The vector's magnitude / norm.
         */
        readonly magnitude: number;

        /**
         * The vector's squared magnitude / squared norm.
         */
        readonly magnitude2: number;

        /**
         * Creates a normalized vector based on this instance's values.
         */
        readonly normalized: this;

        /**
         * Adds a vector to this instance.
         * @param other The vector to add.
         * @returns This instance.
         */
        add(other: vec2): this;

        /**
         * Subtracts a vector from this instance.
         * @param other The vector to subtract.
         * @returns This instance.
         */
        sub(other: vec2): this;

        /**
         * Multiplies this instance by a vector.
         * @param other The vector to add.
         * @returns This instance.
         */
        mul(other: vec2): this;

        /**
         * Divides this instance by a vector.
         * @param other The vector to divide by.
         * @returns This instance.
         */
        div(other: vec2): this;

        /**
         * Scales this vector by a factor.
         * @param q Scaling factor.
         * @returns this instance.
         */
        scale(q: number): this;

        /**
         * Negates this vector.
         * @returns this instance.
         */
        negate(): this;

        /**
         * X coordinate.
         */
        x: number;

        /**
         * Y coordinate.
         */
        y: number;

    }

    export const vec2: {

        /**
         * Creates a new Vector2 instance.
         * @param x X coordinate.
         * @param y Y coordinate.
         * @constructor
         */
        new(x: number, y: number): vec2;

        /**
         * Add a scaled vector to another vector, useful for integration.
         * @param out Output vector.
         * @param a First vector.
         * @param b Second vector.
         * @param q Scale factor.
         * @returns The output vector.
         */
        prototype: vec2;

        cross(out: vec3, u: vec2, v: vec2): vec3;

        /**
         * Interpolate/Exterpolate between two vectors.
         * @param out Output vector.
         * @param u Start vector value.
         * @param v End vector value.
         * @param t Interplation / exterpolation parameter.
         * @returns The output vector.
         */
        lerp(out: vec2, u: vec2, v: vec2, t: number): vec2;

        /**
         * Add a scaled vector to another vector, useful for integration.
         * @param out Output vector.
         * @param a First vector.
         * @param b Second vector.
         * @param q Scale factor.
         * @returns The output vector.
         */
        addScaled(out: vec2, a: vec2, b: vec2, q: number): vec2;

        /**
         * Dot product of two vectors.
         * @param u First vector.
         * @param v Second vector.
         * @returns The dot product value.
         */
        dot(u: vec2, v: vec2): number;

        /**
         * Euclidean distance between two vectors.
         * @param u First vector.
         * @param v Second vector.
         * @returns The distance value.
         */
        dist(u: vec2, v: vec2): number;

        /**
         * Squared euclidean distance between two vectors.
         * @param u First vector.
         * @param v Second vector.
         * @returns The squared distance value.
         */
        dist2(u: vec2, v: vec2): number;

        /**
         * Normalize a vector.
         * @param out Output vector.
         * @param u The vector to be normalized.
         * @returns The output vector.
         */
        noramlize(out: vec2, u: vec2): vec2;

        /**
         * Perform vector addition.
         * @param out Output vector.
         * @param u First vector.
         * @param v Second vector.
         * @returns The output vector.
         */
        add(out: vec2, u: vec2, v: vec2): vec2;

        /**
         * Perform vector subtraction.
         * @param out Output vector.
         * @param u First vector.
         * @param v Second vector.
         * @returns The output vector.
         */
        sub(out: vec2, u: vec2, v: vec2): vec2;

        /**
         * Perform vector multiplication.
         * @param out Output vector.
         * @param u First vector.
         * @param v Second vector.
         * @returns The output vector.
         */
        mul(out: vec2, u: vec2, v: vec2): vec2;

        /**
         * Perform vector division.
         * @param out Output vector.
         * @param u First vector.
         * @param v Second vector.
         * @returns The output vector.
         */
        div(out: vec2, u: vec2, v: vec2): vec2;

        /**
         * Perform vector scalar product.
         * @param out Output vector.
         * @param u The vector value.
         * @param q Scaling factor.
         * @returns The output vector.
         */
        scale(out: vec2, u: vec2, q: number): vec2;

        /**
         * Negate a vector.
         * @param out Output vector.
         * @param u The vector value.
         * @returns The output vector.
         */
        negate(out: vec2, u: vec2): vec2;

    };

    //#endregion

    //#region vec3 defintion

    export interface vec3 extends Float32Array {

        /**
         * @param x X coordinate.
         * @param y Y coordinate.
         * @param z Z coordinate.
         * @returns the current instance.
         */
        setCoords(x: number, y: number, z: number): this;

        /**
         * Resets the coordinates to zero.
         * @returns the current instance.
         */
        resetCoords(): this;

        /**
         * Clones this vector, creating a new instance.
         * @returns the new vector.
         */
        clone(): this;

        /**
         * Adds a scaled vector, useful in integration.
         * @param u Other vector.
         * @param q Scale factor.
         * @returns This vector.
         */
        addScaled(u: vec3, q: number): this;

        /**
         * The vector's magnitude / norm.
         */
        readonly magnitude: number;

        /**
         * The vector's squared magnitude / squared norm.
         */
        readonly magnitude2: number;

        /**
         * Creates a normalized vector based on this instance's values.
         */
        readonly normalized: this;

        /**
         * Adds a vector to this instance.
         * @param other The vector to add.
         * @returns This instance.
         */
        add(other: vec3): this;

        /**
         * Subtracts a vector from this instance.
         * @param other The vector to subtract.
         * @returns This instance.
         */
        sub(other: vec3): this;

        /**
         * Multiplies this instance by a vector.
         * @param other The vector to add.
         * @returns This instance.
         */
        mul(other: vec3): this;

        /**
         * Divides this instance by a vector.
         * @param other The vector to divide by.
         * @returns This instance.
         */
        div(other: vec3): this;

        /**
         * Scales this vector by a factor.
         * @param q Scaling factor.
         * @returns this instance.
         */
        scale(q: number): this;

        /**
         * Negates this vector.
         * @returns this instance.
         */
        negate(): this;

        /**
         * X coordinate.
         */
        x: number;

        /**
         * Y coordinate.
         */
        y: number;

        /**
         * Z coordinate.
         */
        z: number;

    }

    export const vec3: {

        /**
         * Creates a new Vector3 instance.
         * @param x X coordinate.
         * @param y Y coordinate.
         * @param z Z coordinate.
         * @constructor
         */
        new(x: number, y: number, z: number): vec3;
        
        prototype: vec3;

        /**
         * Interpolate/Exterpolate between two vectors.
         * @param out Output vector.
         * @param u Start vector value.
         * @param v End vector value.
         * @param t Interplation / exterpolation parameter.
         * @returns The output vector.
         */
        lerp(out: vec3, u: vec3, v: vec3, t: number): vec3;

        /**
         * Add a scaled vector to another vector, useful for integration.
         * @param out Output vector.
         * @param a First vector.
         * @param b Second vector.
         * @param q Scale factor.
         * @returns The output vector.
         */
        addScaled(out: vec3, a: vec3, b: vec3, q: number): vec3;

        /**
         * Dot product of two vectors.
         * @param u First vector.
         * @param v Second vector.
         * @returns The dot product value.
         */
        dot(u: vec3, v: vec3): number;

        /**
         * Euclidean distance between two vectors.
         * @param u First vector.
         * @param v Second vector.
         * @returns The distance value.
         */
        dist(u: vec3, v: vec3): number;

        /**
         * Squared euclidean distance between two vectors.
         * @param u First vector.
         * @param v Second vector.
         * @returns The squared distance value.
         */
        dist2(u: vec3, v: vec3): number;

        /**
         * Normalize a vector.
         * @param out Output vector.
         * @param u The vector to be normalized.
         * @returns The output vector.
         */
        noramlize(out: vec3, u: vec3): vec3;

        /**
         * Perform vector addition.
         * @param out Output vector.
         * @param u First vector.
         * @param v Second vector.
         * @returns The output vector.
         */
        add(out: vec3, u: vec3, v: vec3): vec3;

        /**
         * Perform vector subtraction.
         * @param out Output vector.
         * @param u First vector.
         * @param v Second vector.
         * @returns The output vector.
         */
        sub(out: vec3, u: vec3, v: vec3): vec3;

        /**
         * Perform vector multiplication.
         * @param out Output vector.
         * @param u First vector.
         * @param v Second vector.
         * @returns The output vector.
         */
        mul(out: vec3, u: vec3, v: vec3): vec3;

        /**
         * Perform vector division.
         * @param out Output vector.
         * @param u First vector.
         * @param v Second vector.
         * @returns The output vector.
         */
        div(out: vec3, u: vec3, v: vec3): vec3;

        /**
         * Perform vector scalar product.
         * @param out Output vector.
         * @param u The vector value.
         * @param q Scaling factor.
         * @returns The output vector.
         */
        scale(out: vec3, u: vec3, q: number): vec3;

        /**
         * Negate a vector.
         * @param out Output vector.
         * @param u The vector value.
         * @returns The output vector.
         */
        negate(out: vec3, u: vec3): vec3;

    };

    //#endregion
>>>>>>> e98f23a... toString, addScaled, I, J, K, L, UNIT, cross:index.d.ts

    export namespace Vector3 {
        export function create(x: number, y: number, z: number): Vector3;
        export const Zero: Vector3;
        export const UnitX: Vector3;
        export const UnitY: Vector3;
        export const UnitZ: Vector3;
        export function add(out: Vector3, a: Vector3, b: Vector3): Vector3;
        export function sub(out: Vector3, a: Vector3, b: Vector3): Vector3;
        export function div(out: Vector3, a: Vector3, b: Vector3): Vector3;
        export function mul(out: Vector3, a: Vector3, b: Vector3): Vector3;
        export function scale(out: Vector3, u: Vector3, scale: number): Vector3;
        export function iterate(out: Vector3, x: Vector3, dx: Vector3, dt: number): Vector3;
        export function dist(a: Vector3, b: Vector3): number;
        export function check_dist(dist: number, a: Vector3, b: Vector3): boolean;
        export function dot(a: Vector3, b: Vector3): number;
        export function cross(out: Vector3, a: Vector3, b: Vector3): Vector3;
        export function lerp(out: Vector3, a: Vector3, b: Vector3): Vector3;
        export function normalize(out: Vector3, u: Vector3): Vector3;
    }

}