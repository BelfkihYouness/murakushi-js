export class vec4 extends Float32Array {

    constructor(x, y, z, w) { super(3);
        this[0] = x;
        this[1] = y;
        this[2] = z;
        this[3] = w;
    }

    setCoords(x, y, z, w) { this[0] = x; this[1] = y; this[2] = z; this[3] = w; return this; }
    resetCoords() { this[0] = 0; this[1] = 0; this[2] = 0; this[3] = 0; return this; }

    toString() {
        return `vec4(${
            this[0].toFixed(2) }, ${
            this[1].toFixed(2) }, ${
            this[2].toFixed(2) }, ${
            this[3].toFixed(2)
        })`;
    }

    static get ZERO() {
        return new vec4(0, 0, 0, 0);
    }

    static get I() {
        return new vec4(1, 0, 0, 0);
    }

    static get J() {
        return new vec4(0, 1, 0, 0);
    }

    static get K() {
        return new vec4(0, 0, 1, 0);
    }

    static get L() {
        return new vec4(0, 0, 0, 1);
    }

    static get UNIT() {
        return new vec4(1, 1, 1, 1);
    }

    assign(other) {
        this[0] = other[0];
        this[1] = other[1];
        this[2] = other[2];
        this[3] = other[3];
        return this;
    }

    clone() {
        return new vec4(this[0], this[1], this[2], this[3]);
    }

    static lerp(o, u, v, t) {
        const s = 1 - t;
        o[0] = u[0] * s + v[0] * t;
        o[1] = u[1] * s + v[1] * t;
        o[2] = u[2] * s + v[2] * t;
        o[3] = u[3] * s + v[3] * t;
        return o;
    }

    static addScaled(o, a, b, q) {
        o[0] = a[0] + b[0] * q;
        o[1] = a[1] + b[1] * q;
        o[2] = a[2] + b[2] * q;
        o[3] = a[3] + b[3] * q;
        return o;
    }

    addScaled(u, q) {
        this[0] += u[0] * q;
        this[1] += u[1] * q;
        this[2] += u[2] * q;
        this[3] += u[3] * q;
        return this;
    }

    //#region Dot, and distance
    static dot(u, v) { return u[0] * v[0] + u[1] * v[1] + u[2] * v[2] + u[3] * v[3]; }
    static dist(u, v) { return Math.hypot(u[0] - v[0], u[1] - v[1], u[2] - v[2], u[3] - v[3]); }
    static dist2(u, v) { return (u[0] - v[0]) **2 + (u[1] - v[1]) ** 2 + (u[2] - v[2]) ** 2 + (u[3] - v[3]) ** 2; }
    //#endregion

    //#region Magnitude, and normalization.
    get magnitude() { return Math.hypot(this[0], this[1], this[2], this[3]); }
    get magnitude2() { return this[0] ** 2 + this[1] ** 2 + this[2] ** 2 + this[3] ** 2;}
    get normalized() {
        const q = this.magnitude2;
        if (q === 0) return new vec4(0, 0, 0, 0);
        const r = 1 / Math.sqrt(q);
        return new vec4(u[0] * r, u[1] * r, u[2] * r, u[3] * r);
    }

    static normalize(o, u) {
        const q = u.magnitude2;
        if (q === 0) return o.resetCoords();
        const r = 1 / Math.sqrt(q);
        return this.setCoords(u[0] * r, u[1] * r, u[2] * r, u[3] * r);
    }

    normalize() {
        const q = this.magnitude2;
        if (q === 0) return this.resetCoords();
        return this.scale(1 / Math.sqrt(q));
    }

    //#endregion

    //#region Static arithmetic operations
    static add(o, u, v) { o[0] = u[0] + v[0]; o[1] = u[1] + v[1]; o[2] = u[2] + v[2]; o[3] = u[3] + v[3]; return o; }
    static sub(o, u, v) { o[0] = u[0] - v[0]; o[1] = u[1] - v[1]; o[2] = u[2] - v[2]; o[3] = u[3] - v[3]; return o; }
    static mul(o, u, v) { o[0] = u[0] * v[0]; o[1] = u[1] * v[1]; o[2] = u[2] * v[2]; o[3] = u[3] * v[3]; return o; }
    static div(o, u, v) { o[0] = u[0] / v[0]; o[1] = u[1] / v[1]; o[2] = u[2] / v[2]; o[3] = u[3] / v[3]; return o; }
    static scale(o, u, q) { o[0] = u[0] * q; o[1] = u[1] * q; o[2] = u[2] * q; o[3] = u[3] * q; return o; }
    static negate(o, u) { o[0] = -u[0]; o[1] = -u[1]; o[2] = -u[2]; o[3] = -u[3]; return o; }
    //#endregion

    //#region Instance arithmetic operations
    add(u) { this[0] += u[0]; this[1] += u[1]; this[2] += u[2]; this[3] += u[3]; return this; }
    sub(u) { this[0] -= u[0]; this[1] -= u[1]; this[2] -= u[2]; this[3] -= u[3]; return this; }
    mul(u) { this[0] *= u[0]; this[1] *= u[1]; this[2] *= u[2]; this[3] *= u[3]; return this; }
    div(u) { this[0] /= u[0]; this[1] /= u[1]; this[2] /= u[2]; this[3] /= u[3]; return this; }
    scale(q) { this[0] *= q; this[1] *= q; this[2] *= q; this[3] *= q; return this; }
    negate() { this[0] = -this[0]; this[1] = -this[1]; this[2] = -this[2]; this[3] = -this[3]; return this; }
    //#endregion

    //#region Coords getters & setters
    get x() { return this[0]; }
    get y() { return this[1]; }
    get z() { return this[2]; }
    get w() { return this[3]; }
    set x(value) { this[0] = value; }
    set y(value) { this[1] = value; }
    set z(value) { this[2] = value; }
    set w(value) { this[3] = value; }
    //#endregion

}